Se utilizo como db MSSQL.
El API quedo publicado en: http://www.testol.somee.com/swagger/index.html

Notas: al ser un host gratuito no se garantiza la permanencia en el tiempo, si al momento de probarlo no esta online avisar para realizar revision.

El ejercicio llevo 10 horas, esto abarca, investigacion sobre acceso a la API de outlook, desarrollo del ejercicio, publicacion de la API.

Aclaracion: 
La sincronizacion con outlook se realiza en forma real o emulada, modificando el parametro Emulated desde el appsettings, si el mismo es true, la api funciona sin la sincronizacion, si es false entonces realiza la sincronizacion con outlook.
Se agrego esta opcion ya que en lo personal no dispongo de una cuenta de azure para realizar las pruebas.
