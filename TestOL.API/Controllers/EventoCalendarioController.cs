﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TestOL.Domain.DTOs;
using TestOL.Logic.Interfaces;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TestOL.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EventoCalendarioController : ControllerBase
    {
        readonly IEventCalendarService _eventCalendarService;

        public EventoCalendarioController(IEventCalendarService eventCalendarService)
        {
            _eventCalendarService = eventCalendarService;
        }

        [HttpGet]
        public async Task<List<EventCalendarDTO>> Get()
        {
            return await _eventCalendarService.ListAsync();
        }

        [HttpGet("{id}")]
        public async Task<EventCalendarDTO> Get(int id)
        {
            return await _eventCalendarService.GetAsync(id);
        }

        [HttpPost]
        public async Task<int> Post([FromBody] EventCalendarDTO value)
        {
            return await _eventCalendarService.SaveAsync(value);
        }

        [HttpPut("{id}")]
        public async Task Put(int id, [FromBody] EventCalendarDTO value)
        {
            if (id != value.Id) throw new Exception("Error item invalido");
            await _eventCalendarService.SaveAsync(value);
        }

        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            await _eventCalendarService.DeleteAsync(id);
        }
    }
}
