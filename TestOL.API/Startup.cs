using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using AutoMapper;
using TestOL.Logic.Interfaces;
using TestOL.Logic.Services;
using TestOL.DAL.Interfaces;
using TestOL.DAL.Implementation;
using Microsoft.EntityFrameworkCore;
using TestOL.Domain.Config;

namespace TestOL.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "TestOL.API", Version = "v1" });
            });

            const string ConnectionString = "DefaultConnection";

            services.AddDbContext<TestOLDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString(ConnectionString)));

            services.AddAutoMapper(System.AppDomain.CurrentDomain.GetAssemblies());

            //inyecto configuracion para acceso a outlook api
            var outlookConfigSection = Configuration.GetSection("OutlookClientConfig");
            services.Configure<OutlookClientConfig>(outlookConfigSection);
            services.AddSingleton(outlookConfigSection.Get<OutlookClientConfig>());

            services.AddSingleton<IOutlookClientProvider, OutlookClientProvider>();
            services.AddTransient<IOutlookEventService, OutlookEventService>();
            services.AddTransient<IEventCalendarService, EventCalendarService>();
            services.AddTransient<ITestOLDbContext, TestOLDbContext>();
        }


        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            //if (env.IsDevelopment())
            //{
            app.UseDeveloperExceptionPage();
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                //c.SwaggerEndpoint("/swagger/v1/swagger.json", "StockPyme.API v1");
                string swaggerJsonBasePath = string.IsNullOrWhiteSpace(c.RoutePrefix) ? "." : "..";
                c.SwaggerEndpoint($"{swaggerJsonBasePath}/swagger/v1/swagger.json", "StockPyme.API v1");
            });

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
