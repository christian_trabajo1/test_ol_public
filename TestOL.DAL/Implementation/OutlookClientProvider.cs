﻿using Azure.Identity;
using Microsoft.Graph;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestOL.Domain.Config;
using TestOL.Domain.DTOs;
using AutoMapper;
using TestOL.DAL.Interfaces;

namespace TestOL.DAL.Implementation
{
    public class OutlookClientProvider: IOutlookClientProvider
    {
        private readonly GraphServiceClient graphClient;
        private readonly IMapper _mapper;
        private readonly OutlookClientConfig _config;

        public OutlookClientProvider(OutlookClientConfig config, IMapper mapper)
        {
            _mapper = mapper;
            _config = config;  

            var tenantId = "common";

            var clientId = config.ClientId;

            // using Azure.Identity;
            var options = new TokenCredentialOptions
            {
                AuthorityHost = AzureAuthorityHosts.AzurePublicCloud
            };

            var userName = config.UserName;
            var password = config.Password;

            var userNamePasswordCredential = new UsernamePasswordCredential(
                userName, password, tenantId, clientId, options);

            graphClient = new GraphServiceClient(userNamePasswordCredential, config.GraphUserScopes);
        }

        public async Task<EventCalendarOutlookDTO> CreateEvent(EventCalendarOutlookDTO eventDTO)
        {
            if (_config.Emulated) return eventDTO;

            var eventOL = _mapper.Map<Event>(eventDTO);
            eventOL.AllowNewTimeProposals = true;
            eventOL.TransactionId = Guid.NewGuid().ToString();

            var newEvent = await graphClient.Me.Events
                .Request()
                .Header("Prefer", "outlook.timezone=\"Pacific Standard Time\"")
                .AddAsync(eventOL);

            return _mapper.Map<EventCalendarOutlookDTO>(newEvent); 
        }

        public async Task UpdateEvent(EventCalendarOutlookDTO eventDTO)
        {
            if (_config.Emulated) return;

            var eventOL = _mapper.Map<Event>(eventDTO);

            await graphClient.Me.Events[eventDTO.OutlookId]
                .Request()
                .UpdateAsync(eventOL);
        }

        public async Task<List<EventCalendarOutlookDTO>> ListEvents()
        {
            if (_config.Emulated) return new List<EventCalendarOutlookDTO>();

            var result = await graphClient.Me.Events
                .Request()
                .GetAsync();

            return _mapper.Map<List<EventCalendarOutlookDTO>>(result.ToList());
        }

        public async Task DeleteEvent(string id)
        {
            if (_config.Emulated) return;

            await graphClient.Me.Events[id]
                .Request()
                .DeleteAsync();
        }
    }
}
