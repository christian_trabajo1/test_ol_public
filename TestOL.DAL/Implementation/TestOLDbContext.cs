﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using TestOL.DAL.Interfaces;
using TestOL.Domain.Entities;

namespace TestOL.DAL.Implementation
{
    public class TestOLDbContext : DbContext, ITestOLDbContext
    {
        public DbSet<EventCalendar> EventsCalendar { get; set; }


        public TestOLDbContext(DbContextOptions<TestOLDbContext> options): base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(ITestOLDbContext).Assembly);
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
        }
    }
}
