﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestOL.Domain.DTOs;

namespace TestOL.DAL.Interfaces
{
    public interface IOutlookClientProvider
    {
        /// <summary>
        /// Create an outlook event
        /// </summary>
        /// <param name="eventDTO"></param>
        /// <returns></returns>
        Task<EventCalendarOutlookDTO> CreateEvent(EventCalendarOutlookDTO eventDTO);
        /// <summary>
        /// Update an outlook event
        /// </summary>
        /// <param name="eventDTO"></param>
        /// <returns></returns>
        Task UpdateEvent(EventCalendarOutlookDTO eventDTO);
        /// <summary>
        /// List outlook events
        /// </summary>
        /// <returns></returns>
        Task<List<EventCalendarOutlookDTO>> ListEvents();
        /// <summary>
        /// Delete an outlook event
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task DeleteEvent(string id);

    }
}
