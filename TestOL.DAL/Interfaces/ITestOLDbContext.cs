﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TestOL.Domain.Entities;

namespace TestOL.DAL.Interfaces
{
    public interface ITestOLDbContext
    {
        DbSet<EventCalendar> EventsCalendar { get; set; }

        int SaveChanges();
        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}
