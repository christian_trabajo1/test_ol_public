﻿using AutoMapper;
using Microsoft.Graph;
using TestOL.Domain.DTOs;

namespace TestOL.DAL.Mapper
{
    public class DALMapper: Profile
    {
        public DALMapper() {
            CreateMap<EventCalendarOutlookDTO, Event>()
                .ForMember(
                    src => src.Id,
                    dst => dst.MapFrom(obj => obj.OutlookId)
                )
                .ForMember(
                    src => src.Body,
                    dst => dst.MapFrom(obj => new ItemBody() {
                        ContentType = BodyType.Html,
                        Content = obj.Description
                    })
                )
                .ForMember(
                    src => src.Start,
                    dst => dst.MapFrom(obj => new DateTimeTimeZone()
                    {
                        DateTime = obj.StartDate.ToString("yyyy-MM-ddTHH:mm:ss"),
                        TimeZone = "Pacific Standard Time"
                    })
                )
                .ForMember(
                    src => src.End,
                    dst => dst.MapFrom(obj => new DateTimeTimeZone()
                    {
                        DateTime = obj.EndDate.ToString("yyyy-MM-ddTHH:mm:ss"),
                        TimeZone = "Pacific Standard Time"
                    })
                )
                .ForMember(
                    src => src.Location,
                    dst => dst.MapFrom(obj => new Location()
                    {
                        DisplayName = "Virtual location"
                    })
                )
                .ReverseMap();
        }
    }
}
