﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TestOL.DAL.Migrations
{
    public partial class event_change : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "MeetingDate",
                table: "EventsCalendar",
                newName: "StartDate");

            migrationBuilder.AddColumn<DateTime>(
                name: "EndDate",
                table: "EventsCalendar",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EndDate",
                table: "EventsCalendar");

            migrationBuilder.RenameColumn(
                name: "StartDate",
                table: "EventsCalendar",
                newName: "MeetingDate");
        }
    }
}
