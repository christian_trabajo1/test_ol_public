﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TestOL.DAL.Migrations
{
    public partial class event_change_id : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "OutlookId",
                table: "EventsCalendar",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "OutlookId",
                table: "EventsCalendar");
        }
    }
}
