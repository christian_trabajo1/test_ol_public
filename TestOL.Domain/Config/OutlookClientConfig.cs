﻿namespace TestOL.Domain.Config
{
    public class OutlookClientConfig
    {
        public string ClientId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string[] GraphUserScopes { get; set; }
        public bool Emulated { get; set; }
    }
}
