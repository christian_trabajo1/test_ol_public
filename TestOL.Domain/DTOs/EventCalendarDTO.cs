﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestOL.Domain.DTOs
{
    public class EventCalendarDTO
    {
        public int Id { get; set; }
        public string Subject { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        /// <summary>
        /// assistants with email and name separated coma ex: <emilio.perezabc111@gmail.com>Emilio Perez;<alfredo.rodriguezabc1112@gmail.com>Alfredo Rodriguez;
        /// </summary>
        public string Assistants { get; set; }
    }
}
