﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TestOL.Logic.Interfaces
{
    public interface IDbService<T>
    {
        Task<int> SaveAsync(T item, CancellationToken cancelationToken = default);
        Task DeleteAsync(int id, CancellationToken cancelationToken = default);
        Task<T> GetAsync(int id, CancellationToken cancelationToken = default);
        Task<List<T>> ListAsync(CancellationToken cancelationToken = default);
    }
}
