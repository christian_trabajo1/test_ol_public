﻿using TestOL.Domain.DTOs;


namespace TestOL.Logic.Interfaces
{
    public interface IEventCalendarService: IDbService<EventCalendarDTO>
    {

    }
}
