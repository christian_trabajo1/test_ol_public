﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TestOL.Domain.DTOs;

namespace TestOL.Logic.Interfaces
{
    public interface IOutlookEventService
    {
        /// <summary>
        /// Create an outlook event
        /// </summary>
        /// <param name="eventDTO"></param>
        /// <returns></returns>
        Task<EventCalendarDTO> Create(EventCalendarDTO eventDTO);
        /// <summary>
        /// Update an outlook event
        /// </summary>
        /// <param name="eventDTO"></param>
        /// <returns></returns>
        Task Update(EventCalendarDTO eventDTO);
        /// <summary>
        /// List outlook events
        /// </summary>
        /// <returns></returns>
        Task<List<EventCalendarDTO>> List();
        /// <summary>
        /// Delete an outlook event
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task Delete(string id);
    }
}
