﻿using Microsoft.Graph;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace TestOL.Logic.Mappers
{
    public static class EventCalendarMapper
    {
        public static List<Attendee> ToAttendees(this string formattedAttendees)
        {
            try
            {
                List<Attendee> attendees =
                    formattedAttendees.Split(";").Where(s => s.Trim().Length > 0).ToList().Select(item =>
                    new Attendee
                    {
                        EmailAddress = new EmailAddress
                        {
                            Address = Regex.Split(item, @"\<(.*?)\>")[1],
                            Name = Regex.Split(item, @"\<(.*?)\>")[2]
                        },
                        Type = AttendeeType.Required
                    }

                    ).ToList();

                return attendees;
            }
            catch
            {
                throw new Exception("Invalid attendees string format - correct format is: <email@adress.com>name of attendee;");
            }
        }
    }
}
