﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestOL.Domain.DTOs;
using TestOL.Domain.Entities;

namespace TestOL.Logic.Mappers
{
    public class LogicMapper: Profile
    {
        public LogicMapper()
        {
            CreateMap<EventCalendar, EventCalendarDTO>().ReverseMap();
            CreateMap<EventCalendarDTO, EventCalendarOutlookDTO>()
                .ForMember(
                    src => src.Attendees,
                    dst => dst.MapFrom(x => x.Assistants.ToAttendees())
                )
                .ReverseMap();

        }
    }
}
