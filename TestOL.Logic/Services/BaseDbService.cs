﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestOL.DAL.Interfaces;

namespace TestOL.Logic.Services
{
    public abstract class BaseDbService
    {
        protected readonly ITestOLDbContext _dbContext;
        protected readonly IMapper _mapper;

        public BaseDbService(ITestOLDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

    }
}
