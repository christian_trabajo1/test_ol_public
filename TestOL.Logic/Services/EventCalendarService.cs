﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TestOL.DAL.Interfaces;
using TestOL.Domain.DTOs;
using TestOL.Domain.Entities;
using TestOL.Logic.Interfaces;

namespace TestOL.Logic.Services
{
    public class EventCalendarService: BaseDbService, IEventCalendarService
    {
        readonly IOutlookEventService _outlookEventService;

        public EventCalendarService(ITestOLDbContext dbContext, 
            IOutlookEventService outlookEventService, IMapper mapper) : base(dbContext, mapper) 
        {
            _outlookEventService = outlookEventService;
        }

        public async Task DeleteAsync(int id, CancellationToken cancelationToken = default)
        {
            var item = await _dbContext.EventsCalendar.FindAsync(id);
            if (item != null)
            {
                var outLookId = item.OutlookId;
                _dbContext.EventsCalendar.Remove(item);
                await _dbContext.SaveChangesAsync(cancelationToken);
                if (outLookId != null)
                    await _outlookEventService.Delete(outLookId);
            }           
        }

        public async Task<EventCalendarDTO> GetAsync(int id, CancellationToken cancelationToken = default)
        {
            var result = _mapper.Map<EventCalendarDTO>(await _dbContext.EventsCalendar.FindAsync(id));
            return result;
        }

        public async Task<List<EventCalendarDTO>> ListAsync(CancellationToken cancelationToken = default)
        {
            return _mapper.Map<List<EventCalendarDTO>>(await Task.Run(() => _dbContext.EventsCalendar.ToList()));
        }

        public async Task<int> SaveAsync(EventCalendarDTO item, CancellationToken cancelationToken = default)
        {
            var itemDb = _mapper.Map<EventCalendar>(item);

            if (itemDb.Id == 0)
            {
                await _dbContext.EventsCalendar.AddAsync(itemDb, cancelationToken);
                await _outlookEventService.Create(item);
            }
            else
            {
                _dbContext.EventsCalendar.Update(itemDb);
                await _outlookEventService.Update(item);
            }

            await _dbContext.SaveChangesAsync(cancelationToken);           
            return itemDb.Id;
        }
    }
}
