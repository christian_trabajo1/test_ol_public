﻿using AutoMapper;
using System.Collections.Generic;
using System.Threading.Tasks;
using TestOL.DAL.Interfaces;
using TestOL.Domain.DTOs;
using TestOL.Logic.Interfaces;

namespace TestOL.Logic.Services
{
    public class OutlookEventService : BaseDbService, IOutlookEventService
    {
        readonly IOutlookClientProvider _outlookClientProvider;

        public OutlookEventService(ITestOLDbContext dbContext, IOutlookClientProvider outlookClientProvider, IMapper mapper) : base(dbContext, mapper)
        {
            _outlookClientProvider = outlookClientProvider;
        }

        public async Task<EventCalendarDTO> Create(EventCalendarDTO eventDTO)
        {
            var eventOL = _mapper.Map<EventCalendarOutlookDTO>(eventDTO);
            return _mapper.Map<EventCalendarDTO>(await _outlookClientProvider.CreateEvent(eventOL));           
        }

        public async Task Delete(string outlookId)
        {
            await _outlookClientProvider.DeleteEvent(outlookId);
        }

        public async Task<List<EventCalendarDTO>> List()
        {
            return _mapper.Map<List<EventCalendarDTO>>(await _outlookClientProvider.ListEvents());
        }

        public async Task Update(EventCalendarDTO eventDTO)
        {
            var eventOL = _mapper.Map<EventCalendarOutlookDTO>(eventDTO);
            await _outlookClientProvider.UpdateEvent(eventOL);
        }
    }
}
