using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TestOL.DAL.Implementation;
using TestOL.Domain.Entities;

namespace TestOL.Test
{
    [TestClass]
    public class EventCalendarServiceTest
    {
        [TestMethod]
        public void SaveTest()
        {
            var _contextOptions = new DbContextOptionsBuilder<TestOLDbContext>()
                .UseInMemoryDatabase("TestOL")
                .ConfigureWarnings(b => b.Ignore(InMemoryEventId.TransactionIgnoredWarning))
                .Options;

            using (var context = new TestOLDbContext(_contextOptions))
            {
                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();

                context.EventsCalendar.Add(new EventCalendar() { 
                    Subject = "Test 1", 
                    Description = "Prueba de event calendar ", 
                    Assistants = "<test1@flflflf.com>Test numero1;", 
                    StartDate = DateTime.Now,
                    EndDate = DateTime.Now.AddMinutes(30)
                });
                context.SaveChanges();


                var ec = context.EventsCalendar.Find(1);

                Assert.IsTrue(ec != null);
            }
        }
    }
}
