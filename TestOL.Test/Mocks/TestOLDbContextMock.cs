﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TestOL.DAL.Interfaces;
using TestOL.Domain.Entities;

namespace TestOL.Test.Mocks
{
    internal class TestOLDbContextMock : DbContext, ITestOLDbContext
    {
        public DbSet<EventCalendar> EventsCalendar { get; set; }

        public TestOLDbContextMock(DbContextOptions<TestOLDbContextMock> options) : base(options)
        {
            options = new DbContextOptionsBuilder<TestOLDbContextMock>()
               .UseInMemoryDatabase(databaseName: "TestOLMemory")
               .Options;
        }

    }

}
